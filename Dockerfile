FROM ubuntu:18.04

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

RUN apt-get update -q \
    && apt-get install -y -qq wget openjdk-8-jdk maven git coreutils hmmer python-numpy python-qt4 python-lxml \
    python-six python-pip python-biopython python-requests python3 ncbi-data libdw1
RUN pip install --upgrade ete3

COPY scripts /opt/scripts/bin
RUN chmod 775 /opt/scripts/bin/*.sh

RUN /opt/scripts/bin/update-taxonomy-cache.py
RUN /opt/scripts/bin/download-interproscan.sh
RUN /opt/scripts/bin/download-unifire.sh

RUN mkdir /volume
VOLUME /volume

CMD /opt/scripts/bin/unifire-workflow.sh
