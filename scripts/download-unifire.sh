#!/bin/bash

set -e
set -u

GIT_REPO="/opt/git"

echo "Downloading and building UniFIRE..."
mkdir -p ${GIT_REPO}
cd ${GIT_REPO}
git clone https://gitlab.ebi.ac.uk/uniprot-public/unifire.git
cd unifire
./build.sh > /dev/null 2>&1
echo "Done building UniFIRE."